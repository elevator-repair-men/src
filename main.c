int currentfloor = 1;
int traveldirection = 1;
int desiredfloor = 1;
int travelmath = 0;
typedef enum
{
	DEFAULT,
	DOOROPEN,
	DOORCLOSED,
	TRAVEL,
	UP,
	FLOOR2,
	DOWN,


} eSystemState;


/* Event Handler*/
eSystemState DEFAULT_Handler(void) //Starting state no movement keeping the door open
{
	HAL_GPIO_WritePin(GPIOA, DOOR_LED_Pin, GPIO_PIN_SET);
	return DOORCLOSED;
}
eSystemState DOOROPENHandler(void) //Opens door
{
	HAL_GPIO_WritePin(GPIOA, DOOR_LED_Pin, GPIO_PIN_SET);
	HAL_Delay(2*1000); //2 seconds
	return DEFAULT;

}
eSystemState DOORCLOSEDHandler(void) //Closes door
{
	HAL_Delay(2*1000);
	HAL_GPIO_WritePin(GPIOA, DOOR_LED_Pin, GPIO_PIN_RESET);
	return TRAVEL;
}
eSystemState TRAVEL_Handler(currentfloor, desiredfloor){ //Determines if traveling up or down
	int travelmath = desiredfloor - currentfloor;		 //and determines a travel path based on current floor
	if(travelmath > 0){
		traveldirection = 1;
	}else if(travelmath < 0){
		traveldirection = 2;
	}
	if(currentfloor == 1){
		return UP;
	}else if(currentfloor == 2){
		return FLOOR2;
	}else if(currentfloor == 3){
		return DOWN;
	}
}

eSystemState UPHandler(desiredfloor) //Handles traveling up direction from floor 1
{
	HAL_GPIO_WritePin(GPIOA, DOOR_LED_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOA, TRAVEL_LED_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOA, FLOOR_LED1_Pin, GPIO_PIN_RESET);
	HAL_Delay(5*1000); // 1 second
	HAL_GPIO_WritePin(GPIOA, FLOOR_LED2_Pin, GPIO_PIN_SET);

	if(desiredfloor == 3){
		HAL_GPIO_WritePin(GPIOA, TRAVEL_LED_Pin, GPIO_PIN_SET);
		HAL_GPIO_WritePin(GPIOA, FLOOR_LED2_Pin, GPIO_PIN_RESET);
		HAL_Delay(1*1000); // 1 second
		HAL_GPIO_WritePin(GPIOA, FLOOR_LED3_Pin, GPIO_PIN_SET);
	}else{
	}
	return DOOROPEN;

}
eSystemState FLOOR2Handler(traveldirection) //Handles traveling up or down from floor 2
{
	if(traveldirection == 1){
		HAL_GPIO_WritePin(GPIOA, TRAVEL_LED_Pin, GPIO_PIN_SET);
		HAL_GPIO_WritePin(GPIOA, FLOOR_LED2_Pin, GPIO_PIN_RESET);
		HAL_Delay(1*1000);
		HAL_GPIO_WritePin(GPIOA, FLOOR_LED3_Pin, GPIO_PIN_SET);

	}else{
		HAL_GPIO_WritePin(GPIOA, TRAVEL_LED_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(GPIOA, FLOOR_LED2_Pin, GPIO_PIN_RESET);
		HAL_Delay(1*1000);
		HAL_GPIO_WritePin(GPIOA, FLOOR_LED1_Pin, GPIO_PIN_SET);
	}
	return DOOROPEN;
}

eSystemState DOWNHandler(desiredfloor) //Handles traveling down from floor 3
{
	HAL_GPIO_WritePin(GPIOA, DOOR_LED_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOA, DOOR_LED_Pin, GPIO_PIN_SET);
	HAL_Delay(2*1000);
	HAL_GPIO_WritePin(GPIOB, FLOOR_LED3_Pin, GPIO_PIN_RESET);
	HAL_Delay(1*1000);
	HAL_GPIO_WritePin(GPIOA, FLOOR_LED2_Pin, GPIO_PIN_SET);
	if(desiredfloor == 1){
		HAL_GPIO_WritePin(GPIOA, DOOR_LED_Pin, GPIO_PIN_SET);
		HAL_Delay(2*1000);
		HAL_GPIO_WritePin(GPIOB, FLOOR_LED2_Pin, GPIO_PIN_RESET);
		HAL_Delay(1*1000);
		HAL_GPIO_WritePin(GPIOA, FLOOR_LED1_Pin, GPIO_PIN_SET);
	}else{
	}
	return DOOROPEN;
}

  /* USER CODE BEGIN 2 */
      eSystemState eNextState = DEFAULT;
      /* USER CODE END 2 */
      
    while (1)
      {
        /* USER CODE END WHILE */

        /* USER CODE BEGIN 3 */

    switch(eNextState){
    	case DEFAULT: //Starts the code in default state
    		eNextState = DEFAULT_Handler();
    		break;
    	case TRAVEL: //Travel state that handles based on the button pressed
    		if(HAL_GPIO_ReadPin(GPIOB, FLOOR2_Pin)){
    			desiredfloor = 2;
    		}else if(HAL_GPIO_ReadPin(GPIOB, FLOOR1_Pin)) {
    			desiredfloor = 1;
    		}else if(HAL_GPIO_ReadPin(GPIOB, FLOOR3_Pin)){
    			desiredfloor = 3;
    		}
    		eNextState = TRAVEL_Handler();
    		break;
    	case UP: //Travel up state
    		eNextState = UPHandler(desiredfloor);
    		break;
    	case DOWN: //Travel down state
    		eNextState = DOWNHandler(desiredfloor);
    		break;
    	case FLOOR2: //Travel up or down between floor 2 state
    		eNextState = FLOOR2Handler(traveldirection);
    		break;
    	case DOOROPEN: //Open door state
    		eNextState = DOOROPENHandler();
    		break;
    	case DOORCLOSED: //Closed door state
    		eNextState = DOORCLOSEDHandler();
    		break;

    }
    /* USER CODE END 3 */
    }